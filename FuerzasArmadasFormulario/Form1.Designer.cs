﻿namespace FuerzasArmadasFormulario
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCrearInfanteria = new System.Windows.Forms.Button();
            this.cbInfanteria = new System.Windows.Forms.ComboBox();
            this.cbCaballeria = new System.Windows.Forms.ComboBox();
            this.cbArtilleria = new System.Windows.Forms.ComboBox();
            this.btnCrearCaballeria = new System.Windows.Forms.Button();
            this.btnCrearArtilleria = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCrearInfanteria
            // 
            this.btnCrearInfanteria.Location = new System.Drawing.Point(193, 11);
            this.btnCrearInfanteria.Name = "btnCrearInfanteria";
            this.btnCrearInfanteria.Size = new System.Drawing.Size(89, 23);
            this.btnCrearInfanteria.TabIndex = 0;
            this.btnCrearInfanteria.Text = "Crear Infanteria";
            this.btnCrearInfanteria.UseVisualStyleBackColor = true;
            // 
            // cbInfanteria
            // 
            this.cbInfanteria.FormattingEnabled = true;
            this.cbInfanteria.Location = new System.Drawing.Point(13, 13);
            this.cbInfanteria.Name = "cbInfanteria";
            this.cbInfanteria.Size = new System.Drawing.Size(121, 21);
            this.cbInfanteria.TabIndex = 1;
            // 
            // cbCaballeria
            // 
            this.cbCaballeria.FormattingEnabled = true;
            this.cbCaballeria.Location = new System.Drawing.Point(13, 73);
            this.cbCaballeria.Name = "cbCaballeria";
            this.cbCaballeria.Size = new System.Drawing.Size(121, 21);
            this.cbCaballeria.TabIndex = 2;
            // 
            // cbArtilleria
            // 
            this.cbArtilleria.FormattingEnabled = true;
            this.cbArtilleria.Location = new System.Drawing.Point(13, 136);
            this.cbArtilleria.Name = "cbArtilleria";
            this.cbArtilleria.Size = new System.Drawing.Size(121, 21);
            this.cbArtilleria.TabIndex = 3;
            // 
            // btnCrearCaballeria
            // 
            this.btnCrearCaballeria.Location = new System.Drawing.Point(193, 71);
            this.btnCrearCaballeria.Name = "btnCrearCaballeria";
            this.btnCrearCaballeria.Size = new System.Drawing.Size(89, 23);
            this.btnCrearCaballeria.TabIndex = 4;
            this.btnCrearCaballeria.Text = "Crear Caballeria";
            this.btnCrearCaballeria.UseVisualStyleBackColor = true;
            // 
            // btnCrearArtilleria
            // 
            this.btnCrearArtilleria.Location = new System.Drawing.Point(193, 134);
            this.btnCrearArtilleria.Name = "btnCrearArtilleria";
            this.btnCrearArtilleria.Size = new System.Drawing.Size(89, 23);
            this.btnCrearArtilleria.TabIndex = 5;
            this.btnCrearArtilleria.Text = "Crear Artilleria";
            this.btnCrearArtilleria.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 205);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Numero de unidades: ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCrearArtilleria);
            this.Controls.Add(this.btnCrearCaballeria);
            this.Controls.Add(this.cbArtilleria);
            this.Controls.Add(this.cbCaballeria);
            this.Controls.Add(this.cbInfanteria);
            this.Controls.Add(this.btnCrearInfanteria);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCrearInfanteria;
        private System.Windows.Forms.ComboBox cbInfanteria;
        private System.Windows.Forms.ComboBox cbCaballeria;
        private System.Windows.Forms.ComboBox cbArtilleria;
        private System.Windows.Forms.Button btnCrearCaballeria;
        private System.Windows.Forms.Button btnCrearArtilleria;
        private System.Windows.Forms.Label label1;
    }
}

