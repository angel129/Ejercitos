﻿namespace BibliotecaDeTropas.Caballeria
{
    public class UnidadCaballeria : Unidad, ICaballeria
    {
        public UnidadCaballeria() : base()
        {
        }

        public string DarApoyo()
        {
            return "Dando apoyo a la infantería";
        }
    }
}
