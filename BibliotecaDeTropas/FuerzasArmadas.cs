﻿using System.Collections.Generic;

namespace BibliotecaDeTropas
{
    public class FuerzasArmadas
    {
        public List<IUnidad> FuerzasTerrestres{ get; set; }
        public FuerzasArmadas()
        {
            FuerzasTerrestres = new List<IUnidad>();
        }
        public void AñadirUnidadTerrestre(IUnidad unidad)
        {
            FuerzasTerrestres.Add(unidad);
        }
        public int ElementosEjercito()
        {
            return FuerzasTerrestres.Count;
        }
        public double GetPotenciaDeFuegoTotal()
        {
            double potenciaDeFuegoTotal = 0;

            foreach (var unidad in FuerzasTerrestres)
            {
                double potenciaUnidad = unidad.GetCapacidadDestruccion();
                potenciaDeFuegoTotal += potenciaUnidad;
            }

            return potenciaDeFuegoTotal;
        }
        public double GetMovilidadTotal()
        {
            double movilidadTotal = 0;

            foreach (var unidad in FuerzasTerrestres)
            {
                double movilidadUnidad = unidad.GetCapacidadMovil();
                movilidadTotal += movilidadUnidad;
            }

            return movilidadTotal;
        }
        public double GetBlindajeTotal()
        {
            double blindajeTotal = 0;

            foreach (var unidad in FuerzasTerrestres)
            {
                double blindajeUnidad = unidad.GetResistenciaAtaque();
                blindajeTotal += blindajeUnidad;
            }

            return blindajeTotal;
        }
        public double GetDineroGastado()
        {
            double dineroTotal = 0;

            foreach (var unidad in FuerzasTerrestres)
            {
                double costeUnidad = unidad.Precio;
                dineroTotal += costeUnidad;
            }

            return dineroTotal;
        }
        public double GetCapacidadMilitarTotal()//falta la formula
        {
            double potenciaTotal = GetPotenciaDeFuegoTotal();
            double blindajeTotal = GetBlindajeTotal();
            double movilidadTotal = GetMovilidadTotal();
            double capacidadMilitarTotal = (potenciaTotal * (movilidadTotal / 2)) / (100 - blindajeTotal);

            return capacidadMilitarTotal;
        }
    }
}