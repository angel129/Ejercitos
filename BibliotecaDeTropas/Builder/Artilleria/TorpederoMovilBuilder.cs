﻿using BibliotecaDeTropas.Artilleria;
using BibliotecaDeTropas.Builder.Interfaces;

namespace BibliotecaDeTropas.Builder.Artilleria
{
    public class TorpederoMovilBuilder : UnidadBuilder, IArtilleriaBuilder
    {
        public override void DarAtaque()
        {
            Unidad.Destructor = Factory.DameAtaque(19);
        }

        public override void DarBlindaje()
        {
            Unidad.Blindado = Factory.DameBlindaje(2);
        }

        public override void DarMovilidad()
        {
            Unidad.Movil = Factory.DameMovilidad(3);
        }

        public override void DarNombre()
        {
            Unidad.Nombre = "Torpedero móvil";
        }

        public override void DarPrecio()
        {
            Unidad.Precio = 1350;
        }

        public override void DefinirUnidad()
        {
            Unidad = new UnidadArtilleria();
        }
    }
}
