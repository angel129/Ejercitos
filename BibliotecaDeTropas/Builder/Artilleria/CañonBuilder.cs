﻿using BibliotecaDeTropas.Artilleria;
using BibliotecaDeTropas.Builder.Interfaces;

namespace BibliotecaDeTropas.Builder.Artilleria
{
    public class CañonBuilder : UnidadBuilder, IArtilleriaBuilder
    {
        public override void DarAtaque()
        {
            Unidad.Destructor = Factory.DameAtaque(14);
        }

        public override void DarBlindaje()
        {
            Unidad.Blindado = Factory.DameBlindaje(0);
        }

        public override void DarMovilidad()
        {
            Unidad.Movil = Factory.DameMovilidad(0);
        }

        public override void DarNombre()
        {
            Unidad.Nombre = "Cañon";
        }

        public override void DarPrecio()
        {
            Unidad.Precio = 1100;
        }

        public override void DefinirUnidad()
        {
            Unidad = new UnidadArtilleria();
        }
    }
}
