﻿namespace BibliotecaDeTropas.Builder
{
    public class UnidadDirector
    {
        private readonly UnidadBuilder _builder;

        public UnidadDirector(UnidadBuilder builder)
        {
            this._builder = builder;
        }

        public void CrearUnidad()
        {
            _builder.DefinirUnidad();
            _builder.DarNombre();
            _builder.DarPrecio();
            _builder.DarBlindaje();
            _builder.DarMovilidad();
            _builder.DarAtaque();
        }
        public Unidad GetUnidad()
        {
            return _builder.Unidad;
        }
    }
}
