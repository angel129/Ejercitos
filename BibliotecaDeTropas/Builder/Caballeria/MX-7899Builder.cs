﻿using BibliotecaDeTropas.Builder.Interfaces;
using BibliotecaDeTropas.Caballeria;

namespace BibliotecaDeTropas.Builder.Caballeria
{
    public class Mx7899Builder : UnidadBuilder, ICaballeriaBuilder
    {
        public override void DarAtaque()
        {
            Unidad.Destructor = Factory.DameAtaque(0);
        }

        public override void DarBlindaje()
        {
            Unidad.Blindado = Factory.DameBlindaje(1.4);
        }

        public override void DarMovilidad()
        {
            Unidad.Movil = Factory.DameMovilidad(4.5);
        }

        public override void DarNombre()
        {
            Unidad.Nombre = "Transporte MX-7899";
        }

        public override void DarPrecio()
        {
            Unidad.Precio = 4200;
        }

        public override void DefinirUnidad()
        {
            Unidad = new UnidadCaballeria();
        }
    }
}
