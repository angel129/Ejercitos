﻿using BibliotecaDeTropas.Builder.Interfaces;
using BibliotecaDeTropas.Caballeria;

namespace BibliotecaDeTropas.Builder.Caballeria
{
    public class Taxin66 : UnidadBuilder, ICaballeriaBuilder
    {
        public override void DarAtaque()
        {
            Unidad.Destructor = Factory.DameAtaque(0);
        }

        public override void DarBlindaje()
        {
            Unidad.Blindado = Factory.DameBlindaje(0);
        }

        public override void DarMovilidad()
        {
            Unidad.Movil = Factory.DameMovilidad(12);
        }

        public override void DarNombre()
        {
            Unidad.Nombre = "Transporte rápido TAXIN-66";
        }

        public override void DarPrecio()
        {
            Unidad.Precio = 1600;
        }

        public override void DefinirUnidad()
        {
            Unidad = new UnidadCaballeria();
        }
    }
}
