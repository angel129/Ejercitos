﻿using BibliotecaDeTropas.Builder.Interfaces;
using BibliotecaDeTropas.Caballeria;

namespace BibliotecaDeTropas.Builder.Caballeria
{
    public class TanqueVb98Builder : UnidadBuilder, ICaballeriaBuilder
    {
        public override void DarAtaque()
        {
            Unidad.Destructor = Factory.DameAtaque(9.8);
        }

        public override void DarBlindaje()
        {
            Unidad.Blindado = Factory.DameBlindaje(4.8);
        }

        public override void DarMovilidad()
        {
            Unidad.Movil = Factory.DameMovilidad(7.3);
        }

        public override void DarNombre()
        {
            Unidad.Nombre = "Tanque de ataque Sombras-VB98";
        }

        public override void DarPrecio()
        {
            Unidad.Precio = 15600;
        }

        public override void DefinirUnidad()
        {
            Unidad = new UnidadCaballeria();
        }
    }
}
