﻿using BibliotecaDeTropas.Builder.Interfaces;
using BibliotecaDeTropas.Infanteria;

namespace BibliotecaDeTropas.Builder.Infanteria
{
    public class AmetralladorBuilder : UnidadBuilder, IInfanteriaBuilder
    {
        public override void DarAtaque()
        {
            Unidad.Destructor = Factory.DameAtaque(10);
        }

        public override void DarBlindaje()
        {
            Unidad.Blindado = Factory.DameBlindaje(0);
        }

        public override void DarMovilidad()
        {
            Unidad.Movil = Factory.DameMovilidad(4);
        }

        public override void DarNombre()
        {
            Unidad.Nombre = "Ametrallador";
        }

        public override void DarPrecio()
        {
            Unidad.Precio = 400;
        }

        public override void DefinirUnidad()
        {
            Unidad = new UnidadInfanteria();
        }
    }
}
