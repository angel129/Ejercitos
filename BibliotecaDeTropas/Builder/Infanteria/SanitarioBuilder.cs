﻿using BibliotecaDeTropas.Builder.Interfaces;
using BibliotecaDeTropas.Infanteria;

namespace BibliotecaDeTropas.Builder.Infanteria
{
    public class SanitarioBuilder : UnidadBuilder, IInfanteriaBuilder
    {
        public override void DarAtaque()
        {
            Unidad.Destructor = Factory.DameAtaque(0);
        }

        public override void DarBlindaje()
        {
            Unidad.Blindado = Factory.DameBlindaje(5);
        }

        public override void DarMovilidad()
        {
            Unidad.Movil = Factory.DameMovilidad(7);
        }

        public override void DarNombre()
        {
            Unidad.Nombre = "Sanitario";
        }

        public override void DarPrecio()
        {
            Unidad.Precio = 500;
        }

        public override void DefinirUnidad()
        {
            Unidad = new UnidadInfanteria();
        }
    }
}
