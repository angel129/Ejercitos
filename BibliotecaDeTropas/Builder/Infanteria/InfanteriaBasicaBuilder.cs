﻿using BibliotecaDeTropas.Builder.Interfaces;
using BibliotecaDeTropas.Infanteria;

namespace BibliotecaDeTropas.Builder.Infanteria
{
    internal class InfanteriaBasicaBuilder : UnidadBuilder, IInfanteriaBuilder
    {
        public override void DarAtaque()
        {
            Unidad.Destructor = Factory.DameAtaque(7);
        }

        public override void DarBlindaje()
        {
            Unidad.Blindado = Factory.DameBlindaje(0);
        }

        public override void DarMovilidad()
        {
            Unidad.Movil = Factory.DameMovilidad(6);
        }

        public override void DarNombre()
        {
            Unidad.Nombre = "Infantería Básica";
        }

        public override void DarPrecio()
        {
            Unidad.Precio = 250;
        }

        public override void DefinirUnidad()
        {
            Unidad = new UnidadInfanteria();
        }
    }
}
