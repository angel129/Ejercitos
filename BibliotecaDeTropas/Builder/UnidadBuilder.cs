﻿using BibliotecaDeTropas.Factory.AbstractFactory;
namespace BibliotecaDeTropas.Builder
{
    public abstract class UnidadBuilder
    {
        public Unidad Unidad { get; set; }
        public UnidadSinDaños Factory { get; set; } = new UnidadSinDaños();

        public abstract void DefinirUnidad();
        public abstract void DarBlindaje();
        public abstract void DarAtaque();
        public abstract void DarMovilidad();
        public abstract void DarPrecio();
        public abstract void DarNombre();
    }
}
