﻿namespace BibliotecaDeTropas.Infanteria
{
    public class UnidadInfanteria : Unidad, IInfanteria
    {
        public UnidadInfanteria() : base()
        {
        }

        public string TomarTerritorio()
        {
            return "Tomando territorio";
        }
    }
}
