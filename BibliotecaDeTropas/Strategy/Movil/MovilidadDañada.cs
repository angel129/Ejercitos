﻿namespace BibliotecaDeTropas.Strategy.Movil
{
    public class MovilidadDañada : Movil
    {
        public MovilidadDañada(double capacidadMovil) : base(capacidadMovil)
        {

        }
        public override double CapacidadDeMovimiento(Operacion operacion)
        {
            if (operacion == Operacion.ObtenerDatos)
            {
                return CapacidadMovil / 2;
            }
            else
            {
                return CapacidadMovil;
            }
        }
    }
}
