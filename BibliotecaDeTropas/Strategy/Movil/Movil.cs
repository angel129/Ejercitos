﻿namespace BibliotecaDeTropas.Strategy.Movil
{
    public abstract class Movil
    {
        public double CapacidadMovil { get; }

        protected Movil(double capacidadMovil)
        {
            CapacidadMovil = capacidadMovil;
        }

        public abstract double CapacidadDeMovimiento(Operacion operacion);
    }
}