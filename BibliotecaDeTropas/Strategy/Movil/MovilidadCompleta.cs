﻿namespace BibliotecaDeTropas.Strategy.Movil
{
    public class MovilidadCompleta : Movil
    {
        public MovilidadCompleta (double capacidadMovil) : base(capacidadMovil)
        {

        }
        public override double CapacidadDeMovimiento(Operacion operacion)
        {
            return CapacidadMovil;
        }
    }
}
