﻿namespace BibliotecaDeTropas.Strategy.Movil
{
    public class SinMovilidad : Movil
    {
        public SinMovilidad(double capacidadMovil) : base(capacidadMovil)
        {

        }
        public override double CapacidadDeMovimiento(Operacion operacion)
        {
            if (operacion == Operacion.ObtenerDatos)
            {
                return 0;
            }
            else
            {
                return CapacidadMovil;
            }
        }
    }
}
