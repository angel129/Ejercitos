﻿namespace BibliotecaDeTropas.Strategy.Blindaje
{
    public class BlindajeDañado : Blindado
    {

        public BlindajeDañado(double capacidadBlindaje) : base(capacidadBlindaje)
        {
        }

        public override double ResistenciaAlAtaque(Operacion operacion)
        {
            if (operacion == Operacion.ObtenerDatos)
            {
                return CapacidadBlindaje / 2;
            }
            else
            {
                return CapacidadBlindaje;
            }
        }
    }
}
