﻿namespace BibliotecaDeTropas.Strategy.Blindaje
{
    public class SinBlindaje : Blindado
    {

        public SinBlindaje(double capacidadBlindaje) : base(capacidadBlindaje)
        {
        }

        public override double ResistenciaAlAtaque(Operacion operacion)
        {
            if (operacion == Operacion.ObtenerDatos)
            {
                return 0;
            }
            else
            {
                return CapacidadBlindaje;
            }
        }
    }
}
