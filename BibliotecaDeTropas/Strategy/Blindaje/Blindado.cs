﻿namespace BibliotecaDeTropas.Strategy.Blindaje
{
    public abstract class Blindado
    {
        public double CapacidadBlindaje { get; }

        protected Blindado(double capacidadBlindaje)
        {
            CapacidadBlindaje = capacidadBlindaje;
        }

        public abstract double ResistenciaAlAtaque(Operacion operacion);
    }
}