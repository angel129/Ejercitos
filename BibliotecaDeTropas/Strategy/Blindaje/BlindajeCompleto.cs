﻿namespace BibliotecaDeTropas.Strategy.Blindaje
{
    public class BlindajeCompleto : Blindado
    {
        public BlindajeCompleto(double capacidadBlindaje) : base (capacidadBlindaje)
        {
        }

        public override double ResistenciaAlAtaque(Operacion operacion)
        {
            return CapacidadBlindaje;
        }
    }
}
