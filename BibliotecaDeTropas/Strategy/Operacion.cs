﻿namespace BibliotecaDeTropas.Strategy
{
    public enum Operacion
    {
        ObtenerDatos = 1,
        CambiarValor = 2
    }
}
