﻿namespace BibliotecaDeTropas.Strategy.Destructor
{
    public class AtaqueDañado : Destructor
    {
        public AtaqueDañado(double ataque) : base(ataque)
        {

        }
        public override double CapacidadDeDestruccion(Operacion operacion)
        {
            if (operacion == Operacion.ObtenerDatos)
            {
                return Ataque / 2;
            }
            else
            {
                return Ataque;
            }
        }
    }
}
