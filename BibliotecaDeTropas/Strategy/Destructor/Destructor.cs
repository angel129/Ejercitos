﻿namespace BibliotecaDeTropas.Strategy.Destructor
{
    public abstract class Destructor
    {
        public double Ataque { get; }

        protected Destructor(double ataque)
        {
            Ataque = ataque;
        }

        public abstract double CapacidadDeDestruccion(Operacion operacion);
    }
}