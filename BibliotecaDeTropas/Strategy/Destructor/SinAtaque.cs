﻿namespace BibliotecaDeTropas.Strategy.Destructor
{
    public class SinAtaque : Destructor
    {
        public SinAtaque(double ataque) : base(ataque)
        {

        }
        public override double CapacidadDeDestruccion(Operacion operacion)
        {
            if (operacion == Operacion.ObtenerDatos)
            {
                return 0;
            }
            else
            {
                return Ataque;
            }
        }
    }
}
