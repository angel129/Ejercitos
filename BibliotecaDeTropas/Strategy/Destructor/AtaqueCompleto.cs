﻿namespace BibliotecaDeTropas.Strategy.Destructor
{
    public class AtaqueCompleto : Destructor
    {
        public AtaqueCompleto(double ataque) : base(ataque)
        {

        }
        public override double CapacidadDeDestruccion(Operacion operacion)
        {
            return Ataque;
        }
    }
}
