﻿using BibliotecaDeTropas.Builder.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace BibliotecaDeTropas.Factory.FactoryReflection
{
    public class CaballeriaFactoryReflection
    {
        private static Dictionary<String, Type> _tiposInstanciables = new Dictionary<string, Type>();
        public static List<String> Automatico()
        {
            _tiposInstanciables = (from tipo in Assembly.GetExecutingAssembly().GetTypes()
                                  where tipo.GetInterface(typeof(ICaballeriaBuilder).ToString()) != null
                                  select tipo).ToDictionary(t => t.ToString(), t => t);
            return _tiposInstanciables.Keys.ToList<string>();
        }
        private static Type ObtenerTipo(string nombreTipo)
        {
            return (_tiposInstanciables.ContainsKey(nombreTipo) ? _tiposInstanciables[nombreTipo] : null);
        }
        public static ICaballeriaBuilder CreateInstance(string tipoCaballeria)
        {
            ICaballeriaBuilder resultado = (ICaballeriaBuilder)Activator.CreateInstance(ObtenerTipo(tipoCaballeria));

            return resultado;
        }
    }
}
