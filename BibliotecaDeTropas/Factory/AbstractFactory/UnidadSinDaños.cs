﻿using BibliotecaDeTropas.Strategy.Blindaje;
using BibliotecaDeTropas.Strategy.Destructor;
using BibliotecaDeTropas.Strategy.Movil;
namespace BibliotecaDeTropas.Factory.AbstractFactory
{
    public class UnidadSinDaños : IEjercitoTierraAbstractFactory
    {
        public Destructor DameAtaque(double ataque)
        {
            return new DestructorFactory().DameDestructor(Estado.Completo, ataque);
        }

        public Blindado DameBlindaje(double blindaje)
        {
            return new BlindajeFactory().DameBlindaje(Estado.Completo, blindaje);
        }

        public Movil DameMovilidad(double movilidad)
        {
            return new MovilFactory().DameMovilidad(Estado.Completo, movilidad);
        }
    }
}
