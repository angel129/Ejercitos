﻿using BibliotecaDeTropas.Strategy.Blindaje;
using BibliotecaDeTropas.Strategy.Destructor;
using BibliotecaDeTropas.Strategy.Movil;

namespace BibliotecaDeTropas.Factory.AbstractFactory
{
    public interface IEjercitoTierraAbstractFactory
    {
        public Blindado DameBlindaje(double blindaje);
        public Movil DameMovilidad(double movilidad);
        public Destructor DameAtaque(double ataque);
    }
}
