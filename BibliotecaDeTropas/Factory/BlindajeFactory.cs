﻿using BibliotecaDeTropas.Strategy.Blindaje;

namespace BibliotecaDeTropas.Factory
{
    public class BlindajeFactory
    {
        public Blindado DameBlindaje(Estado e, double capacidadBlindaje)
        {
            switch (e)
            {
                case Estado.Completo:
                    return new BlindajeCompleto(capacidadBlindaje);
                case Estado.Dañado:
                    return new BlindajeDañado(capacidadBlindaje);
                case Estado.Destruido:
                    return new SinBlindaje(capacidadBlindaje);
            }
            return null;
        }
    }
}
