﻿using BibliotecaDeTropas.Strategy.Movil;

namespace BibliotecaDeTropas.Factory
{
    public class MovilFactory
    {
        public Movil DameMovilidad(Estado e, double capacidadMovil)
        {
            switch (e)
            {
                case Estado.Completo:
                    return new MovilidadCompleta(capacidadMovil);
                case Estado.Dañado:
                    return new MovilidadDañada(capacidadMovil);
                case Estado.Destruido:
                    return new SinMovilidad(capacidadMovil);
            }
            return null;
        }
    }
}
