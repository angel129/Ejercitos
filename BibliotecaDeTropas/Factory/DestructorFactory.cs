﻿
using BibliotecaDeTropas.Strategy.Destructor;

namespace BibliotecaDeTropas.Factory
{
    public class DestructorFactory
    {
        public Destructor DameDestructor(Estado e, double capacidadDestructiva)
        {
            switch (e)
            {
                case Estado.Completo:
                    return new AtaqueCompleto(capacidadDestructiva);
                case Estado.Dañado:
                    return new AtaqueDañado(capacidadDestructiva);
                case Estado.Destruido:
                    return new SinAtaque(capacidadDestructiva);
            }
            return null;
        }
    }
}
