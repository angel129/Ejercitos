﻿namespace BibliotecaDeTropas.Factory
{
    public enum Estado
    {
        Completo = 1,
        Dañado = 2,
        Destruido = 3
    }
}
