﻿using BibliotecaDeTropas.Strategy.Blindaje;
using BibliotecaDeTropas.Strategy.Destructor;
using BibliotecaDeTropas.Strategy.Movil;
namespace BibliotecaDeTropas
{
    public interface IUnidad
    {
        Blindado Blindado { get; set; }
        Destructor Destructor { get; set; }
        Movil Movil { get; set; }
        string Nombre { get; set; }
        int Precio { get; set; }

        double GetCapacidadDestruccion();
        double GetCapacidadMovil();
        double GetResistenciaAtaque();
    }
}