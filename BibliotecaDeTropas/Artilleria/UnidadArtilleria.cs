﻿

namespace BibliotecaDeTropas.Artilleria
{
    public class UnidadArtilleria : Unidad, IArtilleria
    {
        public UnidadArtilleria() : base()
        {
        }

        public string DarFuegoDeApoyo()
        {
            return "Disparando proyectiles";
        }
    }
}
