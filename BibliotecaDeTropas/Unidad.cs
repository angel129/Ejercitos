﻿using BibliotecaDeTropas.Strategy;
using BibliotecaDeTropas.Strategy.Blindaje;
using BibliotecaDeTropas.Strategy.Destructor;
using BibliotecaDeTropas.Strategy.Movil;
namespace BibliotecaDeTropas
{
    public abstract class Unidad : IUnidad
    {
        public Blindado Blindado { get; set; }
        public Destructor Destructor { get; set; }
        public Movil Movil { get; set; }
        public string Nombre { get; set; }
        public int Precio { get; set; }

        protected Unidad()
        {

        }

        public double GetCapacidadDestruccion()
        {
            return Destructor.CapacidadDeDestruccion(Operacion.ObtenerDatos);
        }

        public double GetCapacidadMovil()
        {
            return Movil.CapacidadDeMovimiento(Operacion.ObtenerDatos);
        }

        public double GetResistenciaAtaque()
        {
            return Blindado.ResistenciaAlAtaque(Operacion.ObtenerDatos); 
        }
    }
}