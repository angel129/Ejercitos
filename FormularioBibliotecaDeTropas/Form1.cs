﻿using BibliotecaDeTropas;
using BibliotecaDeTropas.Builder;
using BibliotecaDeTropas.Builder.Interfaces;
using BibliotecaDeTropas.Factory.FactoryReflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormularioBibliotecaDeTropas
{
    public partial class Form1 : Form
    {
        FuerzasArmadas _fuerzasArmadas;
        public Form1()
        {
            InitializeComponent();
            _fuerzasArmadas = new FuerzasArmadas();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            cbArtilleria.DataSource = ArtilleriaFactoryReflection.Automatico();
            //LimpiarDatasource(cbArtilleria);
            cbInfanteria.DataSource = InfanteriaFactoryReflection.Automatico();
            //LimpiarDatasource(cbInfanteria);
            cbCaballeria.DataSource = CaballeriaFactoryReflection.Automatico();
            //LimpiarDatasource(cbCaballeria);
        }
        private void LimpiarDatasource(ComboBox cb)
        {
            List<string> listaDatos = new List<string>();
            for (int count = 0; count < cb.Items.Count;count++)// var item in cb.Items)
            {
                var item = cb.Items[count];
                var itemDividio = item.ToString().Split(".");
                listaDatos.Add(itemDividio.Last());
            }
            cb.DataSource = listaDatos;
        }

        private void btnCrearInfanteria_Click(object sender, EventArgs e)
        {
            IInfanteriaBuilder infanteria = InfanteriaFactoryReflection.CreateInstance(this.cbInfanteria.SelectedItem.ToString());
            UnidadDirector unidadDirector = new UnidadDirector((UnidadBuilder)infanteria);
            unidadDirector.CrearUnidad();
            _fuerzasArmadas.AñadirUnidadTerrestre(unidadDirector.GetUnidad());
            ActualizarContadores();
        }

        private void btnCrearCaballeria_Click(object sender, EventArgs e)
        {
            ICaballeriaBuilder caballeria = CaballeriaFactoryReflection.CreateInstance(this.cbCaballeria.SelectedItem.ToString());
            UnidadDirector unidadDirector = new UnidadDirector((UnidadBuilder)caballeria);
            unidadDirector.CrearUnidad();
            _fuerzasArmadas.AñadirUnidadTerrestre(unidadDirector.GetUnidad());
            ActualizarContadores();
        }

        private void btnCrearArtilleria_Click(object sender, EventArgs e)
        {
            IArtilleriaBuilder artilleria = ArtilleriaFactoryReflection.CreateInstance(this.cbArtilleria.SelectedItem.ToString());
            UnidadDirector unidadDirector = new UnidadDirector((UnidadBuilder)artilleria);
            unidadDirector.CrearUnidad();
            _fuerzasArmadas.AñadirUnidadTerrestre(unidadDirector.GetUnidad());
            ActualizarContadores();
        }
        private void ActualizarContadores()
        {
            ActualizarContadorUnidades();
            ActualizarPotenciaTotal();
            ActualizarBlindajeTotal();
            ActualizarMovimientoTotal();
            ActualizarCosteTotal();
            ActualizarCapacidadMilitar();
        }
        private void ActualizarContadorUnidades()
        {
            string texto = "Numero de unidades: ";
            int numeroUnidades = _fuerzasArmadas.ElementosEjercito();
            lblNumeroUnidades.Text = texto + numeroUnidades;
        }
        private void ActualizarPotenciaTotal()
        {
            string texto = "Potencia de fuego total: ";
            double potenciaTotal = _fuerzasArmadas.GetPotenciaDeFuegoTotal();
            lblPotencia.Text = texto + potenciaTotal;
        }
        private void ActualizarBlindajeTotal()
        {
            string texto = "Blindaje total: ";
            double blindajeTotal = _fuerzasArmadas.GetBlindajeTotal();
            lblBlindaje.Text = texto + blindajeTotal;
        }
        private void ActualizarMovimientoTotal()
        {
            string texto = "Movimiento total: ";
            double movilidadTotal = _fuerzasArmadas.GetMovilidadTotal();
            lblMovimiento.Text = texto + movilidadTotal;
        }
        private void ActualizarCosteTotal()
        {
            string texto = "Dinero gastado total: ";
            double costeTotal = _fuerzasArmadas.GetDineroGastado();
            lblDineroGastado.Text = texto + costeTotal + "€";
        }
        private void ActualizarCapacidadMilitar()
        {
            string texto = "Capacidad militar: ";
            double costeTotal = _fuerzasArmadas.GetCapacidadMilitarTotal();
            lblCapacidadMilitar.Text = texto + costeTotal;
        }
    }
}
