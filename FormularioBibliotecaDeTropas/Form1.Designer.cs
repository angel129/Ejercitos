﻿namespace FormularioBibliotecaDeTropas
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNumeroUnidades = new System.Windows.Forms.Label();
            this.btnCrearArtilleria = new System.Windows.Forms.Button();
            this.btnCrearCaballeria = new System.Windows.Forms.Button();
            this.cbArtilleria = new System.Windows.Forms.ComboBox();
            this.cbCaballeria = new System.Windows.Forms.ComboBox();
            this.cbInfanteria = new System.Windows.Forms.ComboBox();
            this.btnCrearInfanteria = new System.Windows.Forms.Button();
            this.lblPotencia = new System.Windows.Forms.Label();
            this.lblBlindaje = new System.Windows.Forms.Label();
            this.lblMovimiento = new System.Windows.Forms.Label();
            this.lblDineroGastado = new System.Windows.Forms.Label();
            this.lblCapacidadMilitar = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblNumeroUnidades
            // 
            this.lblNumeroUnidades.AutoSize = true;
            this.lblNumeroUnidades.Location = new System.Drawing.Point(13, 234);
            this.lblNumeroUnidades.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumeroUnidades.Name = "lblNumeroUnidades";
            this.lblNumeroUnidades.Size = new System.Drawing.Size(124, 15);
            this.lblNumeroUnidades.TabIndex = 13;
            this.lblNumeroUnidades.Text = "Numero de unidades: ";
            // 
            // btnCrearArtilleria
            // 
            this.btnCrearArtilleria.Location = new System.Drawing.Point(398, 150);
            this.btnCrearArtilleria.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnCrearArtilleria.Name = "btnCrearArtilleria";
            this.btnCrearArtilleria.Size = new System.Drawing.Size(104, 27);
            this.btnCrearArtilleria.TabIndex = 12;
            this.btnCrearArtilleria.Text = "Crear Artilleria";
            this.btnCrearArtilleria.UseVisualStyleBackColor = true;
            this.btnCrearArtilleria.Click += new System.EventHandler(this.btnCrearArtilleria_Click);
            // 
            // btnCrearCaballeria
            // 
            this.btnCrearCaballeria.Location = new System.Drawing.Point(398, 77);
            this.btnCrearCaballeria.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnCrearCaballeria.Name = "btnCrearCaballeria";
            this.btnCrearCaballeria.Size = new System.Drawing.Size(104, 27);
            this.btnCrearCaballeria.TabIndex = 11;
            this.btnCrearCaballeria.Text = "Crear Caballeria";
            this.btnCrearCaballeria.UseVisualStyleBackColor = true;
            this.btnCrearCaballeria.Click += new System.EventHandler(this.btnCrearCaballeria_Click);
            // 
            // cbArtilleria
            // 
            this.cbArtilleria.FormattingEnabled = true;
            this.cbArtilleria.Location = new System.Drawing.Point(13, 154);
            this.cbArtilleria.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbArtilleria.Name = "cbArtilleria";
            this.cbArtilleria.Size = new System.Drawing.Size(377, 23);
            this.cbArtilleria.TabIndex = 10;
            // 
            // cbCaballeria
            // 
            this.cbCaballeria.FormattingEnabled = true;
            this.cbCaballeria.Location = new System.Drawing.Point(13, 81);
            this.cbCaballeria.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbCaballeria.Name = "cbCaballeria";
            this.cbCaballeria.Size = new System.Drawing.Size(377, 23);
            this.cbCaballeria.TabIndex = 9;
            // 
            // cbInfanteria
            // 
            this.cbInfanteria.FormattingEnabled = true;
            this.cbInfanteria.Location = new System.Drawing.Point(13, 12);
            this.cbInfanteria.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbInfanteria.Name = "cbInfanteria";
            this.cbInfanteria.Size = new System.Drawing.Size(377, 23);
            this.cbInfanteria.TabIndex = 8;
            // 
            // btnCrearInfanteria
            // 
            this.btnCrearInfanteria.Location = new System.Drawing.Point(398, 8);
            this.btnCrearInfanteria.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnCrearInfanteria.Name = "btnCrearInfanteria";
            this.btnCrearInfanteria.Size = new System.Drawing.Size(104, 27);
            this.btnCrearInfanteria.TabIndex = 7;
            this.btnCrearInfanteria.Text = "Crear Infanteria";
            this.btnCrearInfanteria.UseVisualStyleBackColor = true;
            this.btnCrearInfanteria.Click += new System.EventHandler(this.btnCrearInfanteria_Click);
            // 
            // lblPotencia
            // 
            this.lblPotencia.AutoSize = true;
            this.lblPotencia.Location = new System.Drawing.Point(12, 259);
            this.lblPotencia.Name = "lblPotencia";
            this.lblPotencia.Size = new System.Drawing.Size(136, 15);
            this.lblPotencia.TabIndex = 14;
            this.lblPotencia.Text = "Potencia de fuego total: ";
            // 
            // lblBlindaje
            // 
            this.lblBlindaje.AutoSize = true;
            this.lblBlindaje.Location = new System.Drawing.Point(12, 286);
            this.lblBlindaje.Name = "lblBlindaje";
            this.lblBlindaje.Size = new System.Drawing.Size(82, 15);
            this.lblBlindaje.TabIndex = 15;
            this.lblBlindaje.Text = "Blindaje total: ";
            // 
            // lblMovimiento
            // 
            this.lblMovimiento.AutoSize = true;
            this.lblMovimiento.Location = new System.Drawing.Point(12, 313);
            this.lblMovimiento.Name = "lblMovimiento";
            this.lblMovimiento.Size = new System.Drawing.Size(105, 15);
            this.lblMovimiento.TabIndex = 16;
            this.lblMovimiento.Text = "Movimiento total: ";
            // 
            // lblDineroGastado
            // 
            this.lblDineroGastado.AutoSize = true;
            this.lblDineroGastado.Location = new System.Drawing.Point(12, 339);
            this.lblDineroGastado.Name = "lblDineroGastado";
            this.lblDineroGastado.Size = new System.Drawing.Size(120, 15);
            this.lblDineroGastado.TabIndex = 17;
            this.lblDineroGastado.Text = "Dinero gastado total: ";
            // 
            // lblCapacidadMilitar
            // 
            this.lblCapacidadMilitar.AutoSize = true;
            this.lblCapacidadMilitar.Location = new System.Drawing.Point(12, 367);
            this.lblCapacidadMilitar.Name = "lblCapacidadMilitar";
            this.lblCapacidadMilitar.Size = new System.Drawing.Size(106, 15);
            this.lblCapacidadMilitar.TabIndex = 18;
            this.lblCapacidadMilitar.Text = "Capacidad militar: ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblCapacidadMilitar);
            this.Controls.Add(this.lblDineroGastado);
            this.Controls.Add(this.lblMovimiento);
            this.Controls.Add(this.lblBlindaje);
            this.Controls.Add(this.lblPotencia);
            this.Controls.Add(this.lblNumeroUnidades);
            this.Controls.Add(this.btnCrearArtilleria);
            this.Controls.Add(this.btnCrearCaballeria);
            this.Controls.Add(this.cbArtilleria);
            this.Controls.Add(this.cbCaballeria);
            this.Controls.Add(this.cbInfanteria);
            this.Controls.Add(this.btnCrearInfanteria);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label lblNumeroUnidades;
        private Button btnCrearArtilleria;
        private Button btnCrearCaballeria;
        private ComboBox cbArtilleria;
        private ComboBox cbCaballeria;
        private ComboBox cbInfanteria;
        private Button btnCrearInfanteria;
        private Label lblPotencia;
        private Label lblBlindaje;
        private Label lblMovimiento;
        private Label lblDineroGastado;
        private Label lblCapacidadMilitar;
    }
}